from random import randint
from copy import deepcopy
from os import system
from os import name as osname

# os clear
if osname == 'nt':
    clear = "cls"
else:
    clear = "clear"


pole = list("        O O O O O        \n" +\
            "  O O   O   O   O   O O  \n" +\
            "  O O   O   O   O   O O  \n" +\
            "        O   O   O        \n" +\
            "O O O O O   O   O O O O O\n" +\
            "O                       O\n" +\
            "O O O O O       O O O O O\n" +\
            "O                       O\n" +\
            "O O O O O   O   O O O O O\n" +\
            "        O   O   O        \n" +\
            "  O O   O   O   O   O O  \n" +\
            "  O O   O   O   O   O O  \n" +\
            "        O O O O O        ")

# farby
resetcolor = "\033[0m"
# RED, GREEN, YELLOW, BLUE
farby = ["\033[1;91m", "\033[1;92m", "\033[1;93m", "\033[1;94m"]

# všetky políčka tmavšie
for i in range(len(pole)):
    if pole[i] == "O":
        pole[i] = "\033[2mO\033[0m"

# začiatočné políčka
# 0
pole[104] = "\033[2;31mO\033[0m"
# 1
pole[16] = "\033[2;32mO\033[0m"
# 2
pole[232] = "\033[2;33mO\033[0m"
# 3
pole[320] = "\033[2;34mO\033[0m"

# domovské políčka
# 0
pole[28] = pole[30] = pole[54] = pole[56] = "\033[2;31mO\033[0m"
# 1
pole[46] = pole[48] = pole[72] = pole[74] = "\033[2;32mO\033[0m"
# 2
pole[280] = pole[282] = pole[306] = pole[308] = "\033[2;33mO\033[0m"
# 3
pole[262] = pole[264] = pole[288] = pole[290] = "\033[2;34mO\033[0m"

# cieľové políčka
# 0
pole[158] = pole[160] = pole[162] = pole[164] = "\033[2;31mO\033[0m"
# 1
pole[38] = pole[64] = pole[90] = pole[116] = "\033[2;32mO\033[0m"
# 2
pole[178] = pole[176] = pole[174] = pole[172] = "\033[2;33mO\033[0m"
# 3
pole[298] = pole[272] = pole[246] = pole[220] = "\033[2;34mO\033[0m"

novepole = deepcopy(pole)

# okruh
policka = {
        0: 8, 1: 10, 2: 12, 3: 14, 4: 16,
        47: 34, 5: 42,
        46: 60, 6: 68,
        45: 86, 7: 94,
        40: 104, 41: 106, 42: 108, 43: 110, 44: 112, 8: 120, 9: 122, 10: 124, 11: 126, 12: 128,
        39: 130, 13: 154,
        38: 156, 14: 180,
        37: 182, 15: 206,
        36: 208, 35: 210, 34: 212, 33: 214, 32: 216, 20: 224, 19: 226, 18: 228, 17: 230, 16: 232,
        31: 242, 21: 250,
        30: 268, 22: 276,
        29: 294, 23: 302,
        28: 320, 27: 322, 26: 324, 25: 326, 24: 328
        }

# domov
policka.update({
    100: 28, 101: 30, 102: 54, 103: 56,
    200: 46, 201: 48, 202: 72, 203: 74,
    300: 280, 301: 282, 302: 306, 303: 308,
    400: 262, 401: 264, 402: 288, 403: 290
    })

# cieľ
policka.update({
    1000: 158, 1001: 160, 1002: 162, 1003: 164,
    2000: 38, 2001: 64, 2002: 90, 2003: 116,
    3000: 178, 3001: 176, 3002: 174, 3003: 172,
    4000: 298, 4001: 272, 4002: 246, 4003: 220
    })

zaciatok = {
        0: 40,
        1: 4,
        2: 16,
        3: 28
        }


def pokracovat(veta):
    pokracovat = input(veta + " [ENTER]")
    print("")


def cislo(veta):
    while True:
        hodnota = input(veta)
        if hodnota in ["1", "2", "3", "4"]:
            print("")
            return int(hodnota)
        else:
            pokracovat("\nVložiť číslo od 1 po 4 !")


def overflow(cislo):
    if 60 > cislo > 47:
        cislo -= 48
    elif cislo < 0:
        cislo += 48
    return cislo


class Figurka:
    def __init__(self, pozicia, meno):
        self.pozicia = pozicia
        self.meno = meno
        self.konci = False


class Hrac:
    def __init__(self, farba, meno="hráč"):
        self.farba = farba
        self.meno = farba + meno + resetcolor
        self.figurky = [Figurka(0, farba + str(i + 1) + resetcolor) for i in range(4)]

    def poslidomov(self, f):
        index = (hraci.index(self)+1)*100
        for i in range(index, index+4):
            if novepole[policka[i]][7] == "O":
                self.figurky[f].pozicia = i
                self.figurky[f].konci = False
                break

    def tah(self):
        hod = randint(1, 6)
        pokracovat("Hodiť kockou")
        print("Hodil/-a si " + farby[narade] + str(hod) + resetcolor + ".\n", flush=True)
        ktora = cislo(resetcolor + "Posunúť figúrku: " + farby[narade] ) - 1
        figurka = self.figurky[ktora]

        def vyhod(policko, veta):
            # ak políčko nie je prázdne
            if novepole[policka[policko]] != "\033[2mO\033[0m" and novepole[policka[policko]][7] != "O":
                inyhrac = farby.index(novepole[policka[policko]][:7])
                inafigurka = int(novepole[policka[policko]][7]) - 1
                if hraci[inyhrac] == hraci[narade]:
                    pokracovat(veta)
                else:
                    hraci[inyhrac].poslidomov(inafigurka)
                    figurka.pozicia = policko
            else:
                figurka.pozicia = policko


        # ak je v poli
        if figurka.pozicia < 50:
            if (figurka.konci and (overflow(figurka.pozicia + hod) <= zaciatok[narade] - 2 or\
                    overflow(figurka.pozicia + hod) > 44)) or not figurka.konci:
                vyhod(overflow(figurka.pozicia + hod), "Políčko je obsadené.")
            # ak dôjde do cieľa
            elif figurka.konci and overflow(figurka.pozicia + hod) <= zaciatok[narade] + 2:
                vyhod((narade+1)*1000 + overflow(figurka.pozicia + hod) - zaciatok[narade] + 1, "Cieľové políčko je obsadené.")
                if figurka.pozicia > 500:
                    vcieli[narade] += 1
                    if vcieli[narade] == 4:
                        global hraskoncila
                        hraskoncila = True
            else:
                pokracovat("Hodil/-a si príliš veľa.")

            # zistí, či figúrka už ubehla kolo
            if (narade == 1 and (44 < figurka.pozicia < 50 or figurka.pozicia < 4)) or\
                    (narade != 1 and zaciatok[narade] - 8 < figurka.pozicia < zaciatok[narade]):
                figurka.konci = True

        # ak je doma
        elif figurka.pozicia < 500:
            if hod == 6:
                vyhod(zaciatok[narade], "Štartovacie políčko je obsadené.")
            else:
                pokracovat("Musíš hodiť 6.")

        # ak je v cieli
        else:
            pokracovat("Figúrka je v cieli.")


def namiesta():
    global pole, novepole
    novepole = deepcopy(pole)
    for hrac in hraci:
        for figurka in hrac.figurky:
            novepole[policka[figurka.pozicia]] = figurka.meno

    print(resetcolor)
    system(clear)
    print("".join(novepole), "\n")


# hra
def main():
    global hraci, narade, vcieli, hraskoncila

    pocet = cislo("Počet hráčov: ")

    # vytvorí hráčov a dá im farby
    hraci = [Hrac(farby[i]) for i in range(pocet)]

    # koľko figúrok je v cieli
    vcieli = [0, 0, 0, 0]
    hraskoncila = False

    # hráči si vyberú mená a dá figúrkam začiatočné pozície
    for hrac in hraci:
        meno = input(hrac.farba + "Meno hráča " + str(hraci.index(hrac)+1) + ": ")
        if len(meno) != 0:
            hrac.meno = hrac.farba + meno + resetcolor
        print("")
        for i in range(4):
            hrac.figurky[i].pozicia = (hraci.index(hrac)+1)*100 + i

    # vyberie prvého hráča
    narade = randint(0, len(hraci) - 1)

    while not hraskoncila:
        if narade == len(hraci) - 1:
            narade = 0
        else:
            narade += 1

        namiesta()
        print("Na rade je " + hraci[narade].meno + ".\n")

        hraci[narade].tah()
    else:
        namiesta()
        pokracovat("Vyhral/-a " + hraci[narade].meno + ".")


if __name__ == "__main__":
    main()
